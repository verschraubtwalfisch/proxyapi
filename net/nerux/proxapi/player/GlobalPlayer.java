package net.nerux.proxapi.player;

import java.util.Arrays;

import net.nerux.proxapi.bungeecord.BungeecordServer;
import net.nerux.proxapi.command.Rank;
import net.nerux.proxapi.minecraft.MinecraftServer;
import net.nerux.proxapi.netty.protocol.packets.bungeecord.KickPlayerPacket;
import net.nerux.proxapi.netty.protocol.packets.bungeecord.SendPlayerToServerPacket;
import net.nerux.proxapi.netty.protocol.packets.minecraft.DisplayMessagePlayerPlacket;
import net.nerux.proxapi.netty.protocol.packets.minecraft.SendMessagePlayerPacket;
import net.nerux.proxapi.netty.protocol.packets.minecraft.SendSoundPlayerPacket;
import net.nerux.proxapi.netty.protocol.packets.minecraft.TeleportPlayerPacket;

public abstract class GlobalPlayer {

	public abstract String getName();

	public abstract String getUUID();

	public abstract String getTeamSpeakID();

	public abstract MinecraftServer getMinecraftServer();

	public abstract BungeecordServer getBungeecordServer();

	public abstract int getTeamspeakRankID();

	public abstract Rank getMinecraftRank();

	public abstract int getMinecraftRankID();

	public abstract String getSkinValue();
	
	public abstract String getSkinSignature();
	
	public void sendActionBar(String msg) {
		getMinecraftServer()
				.sendPacket(new DisplayMessagePlayerPlacket(Arrays.asList(getName()), Arrays.asList(msg), "actionbar"));
	}

	public void sendTitle(String title, String subtitle) {
		getMinecraftServer().sendPacket(
				new DisplayMessagePlayerPlacket(Arrays.asList(getName()), Arrays.asList(title, subtitle), "title"));
	}

	public void sendMessage(String msg) {
		getMinecraftServer().sendPacket(new SendMessagePlayerPacket(getName(), msg));
	}

	public void sendToServer(String serverName) {
		getBungeecordServer().sendPacket(new SendPlayerToServerPacket(getName(), serverName));
	}

	public void kickPlayer(String reason) {
		getBungeecordServer().sendPacket(new KickPlayerPacket(getName(), reason));
	}

	public void teleport(String worldName, double x, double y, double z, float pitch, float yaw) {
		getMinecraftServer().sendPacket(new TeleportPlayerPacket(getName(), worldName, x, y, z, pitch, yaw));
	}

	public void sendSound(String soundName, float volume, float duration, double x, double y, double z,
			String worldName) {

		getMinecraftServer().sendPacket(
				new SendSoundPlayerPacket(worldName, x, y, z, soundName, volume, duration, Arrays.asList(getName())));
	}

	public abstract void moveTeamspeak(int channelID);

	public abstract void moveTeamspeak(String channelName);

	/*
	 * TODO update rank: - Forum - TeamSpeak - MySQL - global Cache - all local
	 * Caches
	 */
	public abstract void setRank(int tsGroupID, int forumID, int minecraftServerGroupID);

}