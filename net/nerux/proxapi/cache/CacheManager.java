package net.nerux.proxapi.cache;

import java.util.concurrent.TimeUnit;

import org.json.JSONObject;

public abstract class CacheManager<K> {

	public abstract void put(CacheType type, K key, JSONObject obj, int durration, TimeUnit timeUnit);

	public abstract JSONObject get(CacheType type, K key);

	public abstract void onExpire(CacheType type, K key, JSONObject obj);

	public abstract void forceRemove(CacheType type, K key);

	public abstract void clearCache(CacheType type);

}
