package net.nerux.proxapi.netty;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import io.netty.buffer.ByteBuf;

public abstract class NettyPacket {

	private static Charset charset = Charset.forName("UTF-8");

	public abstract void readPacket(ByteBuf byteBuf);

	public abstract void writePacket(ByteBuf byteBuf);

	public void writeStringToByteBuf(ByteBuf byteBuf, String string) {
		byte[] stringBytes = string.getBytes(charset);
		byteBuf.writeInt(stringBytes.length);
		byteBuf.writeBytes(stringBytes);
	}

	public String readString(ByteBuf byteBuf) {
		byte[] stringBytes = new byte[byteBuf.readInt()];
		byteBuf.readBytes(stringBytes);
		return new String(stringBytes, charset);
	}
	
	public void writeStringList(List<String> list, ByteBuf byteBuf) {
		
		byteBuf.writeInt(list.size());

		for (String s : list) {
			writeStringToByteBuf(byteBuf, s);
		}

	}

	public List<String> readStringList(ByteBuf byteBuf) {

		List<String> list = new ArrayList<String>();
		int size = byteBuf.readInt();

		for (int i = 0; i < size; i++) {
			list.add(readString(byteBuf));
		}

		return list;

	}
}
