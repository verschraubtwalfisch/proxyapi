package net.nerux.proxapi.netty.protocol.packets.central;

import java.util.List;

import io.netty.buffer.ByteBuf;
import net.nerux.proxapi.minecraft.inventory.GlobalInventory;
import net.nerux.proxapi.minecraft.inventory.GlobalItem;
import net.nerux.proxapi.minecraft.inventory.GlobalSkullItem;
import net.nerux.proxapi.netty.NettyPacket;

public class InventoryOpenPacket extends NettyPacket {

	private String target;
	private GlobalInventory inv;
	
	public InventoryOpenPacket(String target, GlobalInventory inv){
		this.target = target;
		this.inv = inv;
	}

	public InventoryOpenPacket() {
	}
	
	@Override
	public void writePacket(ByteBuf byteBuf) {
		writeStringToByteBuf(byteBuf, inv.getTitle());
		writeStringToByteBuf(byteBuf, target);
		byteBuf.writeInt(inv.getSize());
		
		for (int i = 0; i < inv.getSize() ; i++){
			
			GlobalItem item = inv.getItem(i);
			
			String name = item.getName();
			List<String> lore = item.getLore();
			int id = item.getId();
			int subID = item.getSubid();
			int amount = item.getAmount();
			

			writeStringToByteBuf(byteBuf, name);
			writeStringList(lore, byteBuf);
			byteBuf.writeInt(id);
			byteBuf.writeInt(subID);
			byteBuf.writeInt(amount);
			
			if(item instanceof GlobalSkullItem){
				byteBuf.writeBoolean(true);
				String skin =((GlobalSkullItem) item).getSkinValue();
				writeStringToByteBuf(byteBuf, skin);
			}else{
				byteBuf.writeBoolean(false);
			}
			
			
		}
		
	}

	@Override
	public void readPacket(ByteBuf byteBuf) {
		String title = readString(byteBuf);
		target = readString(byteBuf);
		int size = byteBuf.readInt();
		
		inv = new GlobalInventory(title, size / 9);
		
		for (int i = 0; i < size; i++){
			String name = readString(byteBuf);
			List<String> lore = readStringList(byteBuf);
			int id = byteBuf.readInt();
			int subID = byteBuf.readInt();
			int amount = byteBuf.readInt();
			
			
			if(byteBuf.readBoolean()){
				inv.setItem(i, new GlobalSkullItem(name, lore, id, 3, amount));
			}else{
				inv.setItem(i, new GlobalItem(name, lore, id, subID, amount));
			}
			
			
		}
		
		
		
		
	}
	
	public String getTarget() {
		return target;
	}
	public GlobalInventory getInventory() {
		return inv;
	}
	
}
