package net.nerux.proxapi.netty.protocol.packets;

import io.netty.buffer.ByteBuf;
import net.nerux.proxapi.netty.NettyPacket;

public class KeepAliveServerPacket extends NettyPacket{

	public KeepAliveServerPacket() {
		
	}
	
	@Override
	public void readPacket(ByteBuf byteBuf) {
		
	}

	@Override
	public void writePacket(ByteBuf byteBuf) {
		
	}

}
