package net.nerux.proxapi.netty.protocol.packets.minecraft;

import io.netty.buffer.ByteBuf;
import net.nerux.proxapi.netty.NettyPacket;

public class TeleportPlayerPacket extends NettyPacket {

	public TeleportPlayerPacket() {
	}

	private String worldname;
	private double x;
	private double y;
	private double z;
	private float pitch;
	private float yaw;
	private String name;

	public TeleportPlayerPacket(String name, String worldname, double x, double y, double z, float pitch, float yaw) {
		this.name = name;
		this.worldname = worldname;
		this.x = x;
		this.y = y;
		this.z = z;
		this.pitch = pitch;
		this.yaw = yaw;
	}

	@Override
	public void readPacket(ByteBuf byteBuf) {
		this.name = readString(byteBuf);
		this.worldname = readString(byteBuf);
		this.x = byteBuf.readDouble();
		this.y = byteBuf.readDouble();
		this.z = byteBuf.readDouble();
		this.pitch = byteBuf.readFloat();
		this.yaw = byteBuf.readFloat();

	}

	@Override
	public void writePacket(ByteBuf byteBuf) {
		writeStringToByteBuf(byteBuf, name);
		writeStringToByteBuf(byteBuf, this.worldname);
		byteBuf.writeDouble(x);
		byteBuf.writeDouble(y);
		byteBuf.writeDouble(z);
		byteBuf.writeFloat(pitch);
		byteBuf.writeFloat(yaw);
	}

	public String getName() {
		return name;
	}

	public float getPitch() {
		return pitch;
	}

	public String getWorldname() {
		return worldname;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public float getYaw() {
		return yaw;
	}

	public double getZ() {
		return z;
	}

}
