package net.nerux.proxapi.netty.protocol.packets.minecraft;

import io.netty.buffer.ByteBuf;
import net.nerux.proxapi.netty.NettyPacket;

public class ReloadServerPacket extends NettyPacket{

	private String reason;

	public ReloadServerPacket(){
		
	}
	
	public ReloadServerPacket(String reason){
		this.reason = reason;
	}
	
	@Override
	public void readPacket(ByteBuf byteBuf) {
		reason = readString(byteBuf);
	}

	@Override
	public void writePacket(ByteBuf byteBuf) {
		writeStringToByteBuf(byteBuf, reason);
	}
	
	public String getReason() {
		return reason;
	}

}
