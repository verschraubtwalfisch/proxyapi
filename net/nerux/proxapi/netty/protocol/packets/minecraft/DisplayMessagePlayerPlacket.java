package net.nerux.proxapi.netty.protocol.packets.minecraft;

import java.util.List;

import io.netty.buffer.ByteBuf;
import net.nerux.proxapi.netty.NettyPacket;

public class DisplayMessagePlayerPlacket extends NettyPacket {

	public DisplayMessagePlayerPlacket() {
	}

	private List<String> target;
	private List<String> message;
	private String mode;

	public DisplayMessagePlayerPlacket(List<String> target, List<String> message, String mode) {
		this.target = target;
		this.message = message;
		this.mode = mode;
	}

	@Override
	public void writePacket(ByteBuf byteBuf) {
		writeStringList(target, byteBuf);
		writeStringList(message, byteBuf);
		writeStringToByteBuf(byteBuf, mode);

	}

	@Override
	public void readPacket(ByteBuf byteBuf) {
		this.target = readStringList(byteBuf);
		this.message = readStringList(byteBuf);
		this.mode = readString(byteBuf);
	}
	
	
	public List<String> getTargetNames(){
		return target;
	}
	
	public List<String> getMessages(){
		return message;
	}
	public String getMode(){
		return mode;
	}
	
	
}
