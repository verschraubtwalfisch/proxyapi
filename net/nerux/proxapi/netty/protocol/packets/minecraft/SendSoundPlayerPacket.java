package net.nerux.proxapi.netty.protocol.packets.minecraft;

import java.util.List;

import io.netty.buffer.ByteBuf;
import net.nerux.proxapi.netty.NettyPacket;

public class SendSoundPlayerPacket extends NettyPacket {

	public SendSoundPlayerPacket() {
	}

	private double x;
	private double y;
	private double z;
	private String worldName;

	private String soundName;
	private float volume;
	private float duration;

	private List<String> target;

	public SendSoundPlayerPacket(String worldName, double x, double y, double z, String soundName, float volume,
			float duration, List<String> target) {
		
		this.worldName = worldName;
		this.x = x;
		this.y = y;
		this.z = z;
		this.soundName = soundName;
		this.volume = volume;
		this.duration = duration;
		this.target = target;
	}

	@Override
	public void writePacket(ByteBuf byteBuf) {
		writeStringToByteBuf(byteBuf, worldName);
		byteBuf.writeDouble(x);
		byteBuf.writeDouble(y);
		byteBuf.writeDouble(z);
		writeStringToByteBuf(byteBuf, soundName);
		byteBuf.writeFloat(volume);
		byteBuf.writeFloat(duration);
		writeStringList(target, byteBuf);
	}
	
	
	@Override
	public void readPacket(ByteBuf byteBuf) {
		this.worldName = readString(byteBuf);
		this.x = byteBuf.readDouble();
		this.y = byteBuf.readDouble();
		this.z = byteBuf.readDouble();
		this.soundName = readString(byteBuf);
		this.volume = byteBuf.readFloat();
		this.duration = byteBuf.readFloat();
		this.target = readStringList(byteBuf);
	}
	
	public String getWorldName(){
		return worldName;
	}
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}

	public float getDuration() {
		return duration;
	}
	public String getSoundName() {
		return soundName;
	}
	
	public List<String> getTarget() {
		return target;
	}
	
	public float getVolume() {
		return volume;
	}
	
}
