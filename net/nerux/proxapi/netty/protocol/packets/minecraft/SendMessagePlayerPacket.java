package net.nerux.proxapi.netty.protocol.packets.minecraft;

import io.netty.buffer.ByteBuf;
import net.nerux.proxapi.netty.NettyPacket;

public class SendMessagePlayerPacket extends NettyPacket {

	private String target;
	private String message;
	
	public SendMessagePlayerPacket() {
	}
	
	public SendMessagePlayerPacket(String target, String message) {
		this.target = target;
		this.message = message;
	}
	
	@Override
	public void readPacket(ByteBuf byteBuf) {
		target = readString(byteBuf);
		message = readString(byteBuf);
	}

	@Override
	public void writePacket(ByteBuf byteBuf) {
		writeStringToByteBuf(byteBuf, target);
		writeStringToByteBuf(byteBuf, message);
	}

}
