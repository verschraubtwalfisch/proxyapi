package net.nerux.proxapi.netty.protocol.packets.bungeecord;

import io.netty.buffer.ByteBuf;
import net.nerux.proxapi.netty.NettyPacket;

public class SendPlayerToServerPacket extends NettyPacket{

	private String target;
	private String server;

	public SendPlayerToServerPacket() {
	}
	public SendPlayerToServerPacket(String target, String server) {
		this.target = target;
		this.server = server;
	}
	
	
	@Override
	public void readPacket(ByteBuf byteBuf) {
		target = readString(byteBuf);
		server = readString(byteBuf);
	}

	@Override
	public void writePacket(ByteBuf byteBuf) {
		writeStringToByteBuf(byteBuf, target);
		writeStringToByteBuf(byteBuf, server);
	}
	
	public String getServer() {
		return server;
	}
	
	public String getTarget() {
		return target;
	}
	
}
