package net.nerux.proxapi.netty.protocol.packets.bungeecord;

import io.netty.buffer.ByteBuf;
import net.nerux.proxapi.netty.NettyPacket;

public class KickPlayerPacket extends NettyPacket {

	private String target;
	private String reason;

	public KickPlayerPacket() {
	}

	public KickPlayerPacket(String target, String reason) {
		this.target = target;
		this.reason = reason;
	}

	@Override
	public void readPacket(ByteBuf byteBuf) {
		target = readString(byteBuf);
		reason = readString(byteBuf);
	}

	@Override
	public void writePacket(ByteBuf byteBuf) {
		writeStringToByteBuf(byteBuf, target);
		writeStringToByteBuf(byteBuf, reason);
	}

}
