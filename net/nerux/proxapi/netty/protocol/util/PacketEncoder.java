package net.nerux.proxapi.netty.protocol.util;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import net.nerux.proxapi.netty.NettyPacket;
import net.nerux.proxapi.netty.protocol.PacketType;

/**
 * Created by max on 15.11.16.
 */
public class PacketEncoder extends MessageToByteEncoder<NettyPacket> {

    @Override
    public void encode(ChannelHandlerContext channelHandlerContext, NettyPacket nettyPacket, ByteBuf byteBuf) throws Exception {

        int packetID = PacketType.getPacketIDByClass(nettyPacket.getClass());

        if (packetID != 0) {
            byteBuf.writeInt(packetID);
            nettyPacket.writePacket(byteBuf);
        }


    }
}
