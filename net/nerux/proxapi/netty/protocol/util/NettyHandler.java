package net.nerux.proxapi.netty.protocol.util;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.nerux.proxapi.netty.NettyPacket;
public class NettyHandler extends SimpleChannelInboundHandler<NettyPacket> {

    private NettyHandlerHelper handlerHelper;

    public NettyHandler(NettyHandlerHelper handlerHelper) {
        this.handlerHelper = handlerHelper;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, NettyPacket nettyPacket) throws Exception {
        handlerHelper.receivePacket(nettyPacket, channelHandlerContext.channel());
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        handlerHelper.channelConnected(ctx.channel());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        handlerHelper.channelDisconnect(ctx.channel());
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        handlerHelper.channelTimeout(ctx.channel());
    }

}