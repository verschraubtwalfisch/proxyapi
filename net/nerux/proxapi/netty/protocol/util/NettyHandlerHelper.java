package net.nerux.proxapi.netty.protocol.util;

import io.netty.channel.Channel;
import net.nerux.proxapi.netty.NettyPacket;

public interface NettyHandlerHelper {
	void receivePacket(NettyPacket nettyPacket, Channel channel);

	void addPacketReceiver(NettyHandlerHelper packetReceiver);

	void channelConnected(Channel channel);

	void channelTimeout(Channel channel);

	void channelDisconnect(Channel channel);
}
