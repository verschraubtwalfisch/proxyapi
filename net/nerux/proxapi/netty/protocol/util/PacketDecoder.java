package net.nerux.proxapi.netty.protocol.util;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import net.nerux.proxapi.netty.NettyPacket;
import net.nerux.proxapi.netty.protocol.PacketType;

import java.util.List;

/**
 * Created by max on 15.11.16.
 */
public class PacketDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {

        int packetID = byteBuf.readInt();

        Class<? extends NettyPacket> nettyPacketClass = PacketType.getPacketByID(packetID);
        if (nettyPacketClass != null) {
            NettyPacket nettyPacket = nettyPacketClass.newInstance();
            nettyPacket.readPacket(byteBuf);
            list.add(nettyPacket);
        } else {
            byteBuf.release();
        }


    }
}
