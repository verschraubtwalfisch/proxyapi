package net.nerux.proxapi.netty.protocol;

import net.nerux.proxapi.netty.NettyPacket;
import net.nerux.proxapi.netty.protocol.packets.KeepAliveServerPacket;
import net.nerux.proxapi.netty.protocol.packets.StopServerPacket;
import net.nerux.proxapi.netty.protocol.packets.bungeecord.KickPlayerPacket;
import net.nerux.proxapi.netty.protocol.packets.bungeecord.SendPlayerToServerPacket;
import net.nerux.proxapi.netty.protocol.packets.minecraft.DisplayMessagePlayerPlacket;
import net.nerux.proxapi.netty.protocol.packets.minecraft.ReloadServerPacket;
import net.nerux.proxapi.netty.protocol.packets.minecraft.SendMessagePlayerPacket;
import net.nerux.proxapi.netty.protocol.packets.minecraft.SendSoundPlayerPacket;
import net.nerux.proxapi.netty.protocol.packets.minecraft.TeleportPlayerPacket;

public enum PacketType {

	KEEPALIVEPACKET(1, KeepAliveServerPacket.class),
	STOPSERVERPACKET(2, StopServerPacket.class),
	RELOADSERVERPACKET(3, ReloadServerPacket.class),
	SENDMESSAGEPACKET(4, SendMessagePlayerPacket.class),
	SENDPLAYERTOSERVERPACKET(5, SendPlayerToServerPacket.class),
	KICKPLAYERPACKET(6, KickPlayerPacket.class),
	TELEPORTPLAYERPACKET(7, TeleportPlayerPacket.class),
	SENDSOUNDPLAYERPACKET(8, SendSoundPlayerPacket.class),
	DISPLAYMESSAGEPLAYERPACKET(9, DisplayMessagePlayerPlacket.class);
	
	
	
	
    private int packetID;

    private Class<? extends NettyPacket> packetClass;

    PacketType(int packetID, Class<? extends NettyPacket> packetClass) {
        this.packetID = packetID;
        this.packetClass = packetClass;

    }

    public Class<? extends NettyPacket> getPacketClass() {
        return packetClass;
    }

    public int getPacketID() {
        return packetID;
    }

    public static int getPacketIDByClass(Class<? extends NettyPacket> packetClass) {

        for (PacketType packetType : values()) {
            if (packetType.getPacketClass().equals(packetClass)) {
                return packetType.getPacketID();
            }
        }
        return 0;
    }

    public static Class<? extends NettyPacket> getPacketByID(int packetID) {
        for (PacketType packetType : values()) {
            if (packetType.getPacketID() == packetID) {
                return packetType.getPacketClass();
            }
        }
        return null;
    }
}
