package net.nerux.proxapi.netty.protocol.boostrap;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.timeout.ReadTimeoutHandler;
import net.nerux.proxapi.netty.NettyPacket;
import net.nerux.proxapi.netty.protocol.util.NettyHandler;
import net.nerux.proxapi.netty.protocol.util.NettyHandlerHelper;
import net.nerux.proxapi.netty.protocol.util.PacketDecoder;
import net.nerux.proxapi.netty.protocol.util.PacketEncoder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by max on 15.11.16.
 */
public class NettyServer implements Runnable, NettyHandlerHelper {

    private List<NettyHandlerHelper> packetReceivers = new ArrayList<NettyHandlerHelper>();
    private NettyHandlerHelper instance;
    private int port;

    public NettyServer(int port) {
        this.instance = this;
        this.port = port;
    }

    public void run() {
        EventLoopGroup bossGroup = new EpollEventLoopGroup();
        EventLoopGroup workerGroup = new EpollEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossGroup, workerGroup)
                    .channel(EpollServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {
							ChannelPipeline pipe = channel.pipeline();
							pipe.addLast("timeout", new ReadTimeoutHandler(5));
							pipe.addLast("splitter", new LengthFieldPrepender(4));
							pipe.addLast("prepender", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
							pipe.addLast("decoder", new PacketDecoder());
							pipe.addLast("encoder", new PacketEncoder());
							pipe.addLast("handler", new NettyHandler(NettyServer.this));
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);
            ChannelFuture channelFuture = serverBootstrap.bind(port).sync();
            System.out.println("Server started on port " + port);
            channelFuture.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public void receivePacket(NettyPacket nettyPacket, Channel channel) {
        synchronized (packetReceivers) {
            for (NettyHandlerHelper packetReceiver : packetReceivers) {
                packetReceiver.receivePacket(nettyPacket, channel);
            }
        }
    }

    public void addPacketReceiver(NettyHandlerHelper packetReceiver) {
        synchronized (packetReceivers) {
            packetReceivers.add(packetReceiver);
        }
    }

    public void channelConnected(Channel channel) {
        synchronized (packetReceivers) {
            for (NettyHandlerHelper packetReceiver : packetReceivers) {
                packetReceiver.channelConnected(channel);
            }
        }
    }

    public void channelTimeout(Channel channel) {
    	synchronized (packetReceivers) {
			for(NettyHandlerHelper helper : packetReceivers){
				helper.channelTimeout(channel);
			}
		}
    }

    public void channelDisconnect(Channel channel) {
        synchronized (packetReceivers) {
            for (NettyHandlerHelper packetReceiver : packetReceivers) {
                packetReceiver.channelDisconnect(channel);
            }
        }
    }
}