package net.nerux.proxapi.netty.protocol.boostrap;

import java.util.ArrayList;
import java.util.List;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.timeout.ReadTimeoutHandler;
import net.nerux.proxapi.netty.NettyPacket;
import net.nerux.proxapi.netty.protocol.util.NettyHandler;
import net.nerux.proxapi.netty.protocol.util.NettyHandlerHelper;
import net.nerux.proxapi.netty.protocol.util.PacketDecoder;
import net.nerux.proxapi.netty.protocol.util.PacketEncoder;

public class NettyClient implements Runnable, NettyHandlerHelper {

	private String host;
	private int port;
	private Channel Channel;
	private List<NettyHandlerHelper> helper = new ArrayList<NettyHandlerHelper>();

	public NettyClient(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public void run() {

		EventLoopGroup workerGroup = new EpollEventLoopGroup();
		try {
			Bootstrap bootstrap = new Bootstrap();
			bootstrap.group(workerGroup).option(ChannelOption.SO_KEEPALIVE, true)
					.handler(new ChannelInitializer<Channel>() {

						@Override
						protected void initChannel(Channel channel) throws Exception {

							ChannelPipeline pipe = channel.pipeline();

							pipe.addLast("timeout", new ReadTimeoutHandler(5));
							pipe.addLast("splitter", new LengthFieldPrepender(4));
							pipe.addLast("prepender", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
							pipe.addLast("decoder", new PacketDecoder());
							pipe.addLast("encoder", new PacketEncoder());
							pipe.addLast("handler", new NettyHandler(NettyClient.this));
						}
					});

		} finally {
			workerGroup.shutdownGracefully();
		}

	}

	public void receivePacket(NettyPacket nettyPacket, io.netty.channel.Channel channel) {

		synchronized (helper) {
			for (NettyHandlerHelper helper : this.helper) {
				helper.receivePacket(nettyPacket, channel);
			}
		}

	}

	public void addPacketReceiver(NettyHandlerHelper packetReceiver) {
		synchronized (helper) {
			helper.add(packetReceiver);
		}
	}

	public void channelConnected(io.netty.channel.Channel channel) {
		synchronized (helper) {
			for (NettyHandlerHelper helper : this.helper) {
				helper.channelConnected(channel);
			}
		}
	}

	public void channelTimeout(io.netty.channel.Channel channel) {
		synchronized (helper) {
			for (NettyHandlerHelper helper : this.helper) {
				helper.channelTimeout(channel);
			}
		}
	}

	public void channelDisconnect(io.netty.channel.Channel channel) {
		synchronized (helper) {
			for (NettyHandlerHelper helper : this.helper) {
				helper.channelDisconnect(channel);
			}
		}
	}

}
