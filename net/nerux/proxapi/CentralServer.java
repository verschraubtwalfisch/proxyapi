package net.nerux.proxapi;

import java.util.List;

import net.nerux.proxapi.player.GlobalPlayer;

public abstract class CentralServer {

	public abstract void start();

	public abstract void startCacheServer();

	public abstract void startMinecraftCommunicationServer();

	public abstract void startBungeecordCommunicationServer();

	public abstract List<GlobalPlayer> getPlayers();

	public abstract int getTotalPlayerCount();

}