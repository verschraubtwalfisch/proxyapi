package net.nerux.proxapi.bungeecord;

import java.util.List;

import io.netty.channel.Channel;
import net.nerux.proxapi.minecraft.MinecraftServer;
import net.nerux.proxapi.netty.NettyPacket;
import net.nerux.proxapi.netty.protocol.packets.StopServerPacket;
import net.nerux.proxapi.player.GlobalPlayer;

public abstract class BungeecordServer {

	public abstract String getServerName();

	public abstract String getMessageOfTheDay();

	public abstract void setMessageOfTheDay(String line1, String line2);

	public abstract String getServerStatus();

	public abstract Channel getChannel();

	public abstract int getPlayerCount();

	public abstract int getMaxPlayers();

	public abstract List<GlobalPlayer> getPlayers();

	public abstract void setTabList(String header, String footer);

	public abstract void addServer(MinecraftServer minecraftServer);

	public abstract void removeServer(String minecraftServerName);

	public void stopServer(String reason) {
		sendPacket(new StopServerPacket(reason));
	}

	public void sendPacket(NettyPacket packet) {
		getChannel().writeAndFlush(packet);
	}

}
