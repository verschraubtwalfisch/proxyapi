package net.nerux.proxapi.command;

public abstract class Command {

	public abstract CommandSender[] getAllowedCommandSender();
	public abstract Rank[] getAllowedRanks();

	String command;
	
	public Command(String command){
		this.command = command;
	}
	
	public String getCommand(){ 
		return command;
	}
	
	public void sendResponse(String id, String response, CommandSender sender){
		
		if(sender == CommandSender.BUNGEECORDCONSOLE){
			//TODO send CommandBungeeConsoleResponsePacket
		}else if(sender ==CommandSender.BUNGEECORDPLAYER){
			//TODO send CommandBungeePlayerResponsePacket
		}else if(sender == CommandSender.CENTRALCONSOLE){
			System.out.println(response);
		}else if(sender == CommandSender.MINECRAFTCONSOLE){
			//TODO send CommandConsoleMinecraftResponsepacket
		}else if(sender == CommandSender.MINECRAFTPLAYER){
			//TODO send CommandPlayerMinecraftResponsePacket
		}else if(sender == CommandSender.SLACKBOT){
			//TODO SlackBot ?
		}else if(sender == CommandSender.TEAMSPEAKBOT){
			//TODO use TS³ API to response
		}
		
	}
	
	public abstract boolean execute(String id, CommandSender sender, String[] args);
	
}
