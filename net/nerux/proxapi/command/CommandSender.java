package net.nerux.proxapi.command;

public enum CommandSender {

	MINECRAFTCONSOLE(),

	MINECRAFTPLAYER(),

	BUNGEECORDCONSOLE(),

	BUNGEECORDPLAYER(),

	TEAMSPEAKBOT(),

	CENTRALCONSOLE(),

	SLACKBOT();

}
