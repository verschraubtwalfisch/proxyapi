package net.nerux.proxapi.command;

import java.util.UUID;

public enum Rank {

	ADMINISTRATOR("§4Admin §8| §4", "§4Admin §8| §4", "§4", "a", -1, 9),

	DEVELOPER("§3Dev §8| §3", "§3Dev §8| §3", "", "b", -1, 13),

	SRMODERATOR("§cSrMod §8| §c", "§cSrMod §8| §c", "", "c", -1, 13),

	MODERATOR("§cMod §8| §c", "§cMod §8| §c", "", "d", -1, 13),

	SUPPORTER("§eSup §8| §e", "§eSup §8| §e", "", "e", -1, 13),

	BUILDER("§eBuilder |§8 §e", "§eBuilder |§8 §e", "", "f", -1, 13),

	YOUTUBER("§5YT §8| §5", "§5YT §8| §5", "", "g", -1, 13),

	PROBESUPPORTER("§ePSup §8| §e", "§ePSup §8| §e", "", "h", -1, 13),

	PREMIUMPLUS("§6", "§6", "", "i", -1, 13), PREMIUM("§6", "§6", "", "j", -1, 13),

	USER("§7", "§7", "", "k", -1, 13);

	private String scoreboardTeamName;
	private String tabPrefix;
	private String chatPrefix;
	private String colorCode;
	private String tabPriority;
	private int forumGroupID;
	private int tsGroupID;

	private Rank(String tabPrefix, String chatPrefix, String colorCode, String tabPriority, int forumGroupID,
			int tsGroupID) {
		this.tabPrefix = tabPrefix;
		this.chatPrefix = chatPrefix;
		this.colorCode = colorCode;
		this.tabPriority = tabPriority;
		this.forumGroupID = forumGroupID;
		this.tsGroupID = tsGroupID;

		scoreboardTeamName = UUID.randomUUID().toString().substring(0, 14);

	}

	public String getChatPrefix() {
		return chatPrefix;
	}

	public String getColorCode() {
		return colorCode;
	}

	public int getForumGroupID() {
		return forumGroupID;
	}

	public String getTabPrefix() {
		return tabPrefix;
	}

	public String getTabPriority() {
		return tabPriority;
	}

	public int getTsGroupID() {
		return tsGroupID;
	}

	public String getScoreboardTeamName() {
		return scoreboardTeamName;
	}

	@Override
	public String toString() {
		return name();
	}

}
