package net.nerux.proxapi.command;

import java.util.ArrayList;
import java.util.List;

public class CommandHandler {

	private List<Command> commands = new ArrayList<Command>();

	public void registerCommand(Command command) {
		commands.add(command);
	}

	public boolean executeCommand(String idOrUserName, CommandSender sender, String command) {

		String[] split = command.split(" ");
		String cmd = split[0];

		for (Command c : commands) {
			if (c.getCommand().equalsIgnoreCase(cmd)) {

				boolean allowed = false;

				for (CommandSender allowedSender : c.getAllowedCommandSender()) {
					if (allowedSender == sender) {
						allowed = true;
						break;
					}
				}
				if (!allowed) {
					return false;
				}

				if (allowed) {

					String[] args = new String[split.length - 1];

					for (int i = 1; i < split.length; i++) {
						args[i - 1] = split[i];
					}
					return c.execute(idOrUserName, sender, args);

				}

			}
		}
		
		return false;
	}

}
