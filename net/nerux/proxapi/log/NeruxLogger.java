package net.nerux.proxapi.log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.nerux.proxapi.command.CommandSender;

public class NeruxLogger {

	private String serverId;
	private CommandSender service;
	private static String path = "/home/logs/";
	private File file;
	private FileWriter writer;

	public NeruxLogger(String serverId, CommandSender service) {
		try {
			this.serverId = serverId;
			this.service = service;

			this.file = new File(path + serverId + "/log.log");
			if (!this.file.exists()) {
				this.file.mkdir();
				this.file.createNewFile();

			}

			this.writer = new FileWriter(this.file);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void log(String senderUUID, String senderName, String log) {
		log(senderUUID, senderName, log, false);
	}

	private void write(String s) {
		SimpleDateFormat format = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss");
		String date = format.format(new Date());

		String log = "[" + date.toString() + "] " + s;

		try {
			writer.write(log + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void error(String log){
		log(null, null, log, true);
	}

	private void log(String senderUUID, String senderName, String log, boolean error) {
		if (error) {
			write("\n\nERROR | "+service.name() + " » " + log + "\n\n");
		} else {
			write(service.name() + " | " + senderUUID + " | (" + senderName + ") » " + log);
		}
	}

	public void cleanUp() {
		try {
			this.serverId = null;
			this.service = null;
			this.writer.close();
			this.file = null;
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
