package net.nerux.proxapi.minecraft.inventory;

import java.util.List;

public class GlobalSkullItem extends GlobalItem{

	private String skinValue;
	
	public GlobalSkullItem(String name, List<String> lore, int id, int subid, int amount) {
		super(name, lore, id, subid, amount);
	}
	
	public void setSkinValue(String skinValue) {
		this.skinValue = skinValue;
	}
	
	public String getSkinValue() {
		return skinValue;
	}
	
}
