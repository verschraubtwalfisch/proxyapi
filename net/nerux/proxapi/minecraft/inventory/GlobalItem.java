package net.nerux.proxapi.minecraft.inventory;

import java.util.Arrays;
import java.util.List;

public class GlobalItem {

	private String name;
	private List<String> lore;
	private int id;
	private int subid;
	private int amount;

	public GlobalItem(int id) {
		this(null, id, 1);
	}

	public GlobalItem(String name, int id, int amount) {
		this(name, null, id, -1, amount);
	}

	public GlobalItem(String name, List<String> lore, int id, int subid, int amount) {
		this.name = name;
		this.lore = lore;
		this.id = id;
		this.subid = subid;
		this.amount = amount;
	}

	public void setName(String s) {
		this.name = s;
	}

	public void addLore(String s) {
		this.lore.add(s);
	}

	public String getName() {
		return (name != null ? name : "");
	}

	public List<String> getLore() {
		return (lore != null ? lore : Arrays.asList(""));
	}

	public int getId() {
		return id;
	}

	public int getSubid() {
		return subid;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getAmount() {
		return this.amount;
	}
}
