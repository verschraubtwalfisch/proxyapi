package net.nerux.proxapi.minecraft.inventory;

public class GlobalInventory {

	private String title;
	private int size;
	private GlobalItem[] items;
	
	private static GlobalItem AIR = new GlobalItem(0);
	
	public GlobalInventory(String title, int rows){
		this.size = rows * 9;
		this.title = title;
		this.items = new GlobalItem[size];
		
		for(int i = 0; i < size; i++){
			items[i] = AIR;
		}
		
	}
	
	public void setItem(int slot, GlobalItem item){
		this.items[slot] = item;
	}
	
	public GlobalItem getItem(int slot){
		return this.items[slot];
	}
	
	public GlobalItem[] getItems() {
		return items;
	}
	
	public int getSize() {
		return size;
	}
	
	public String getTitle() {
		return title;
	}
	
	public enum Types {
		
		LOBBY_SWITCHER,
		BANINVENTORY,
		BANHISTORY,
		PLAYERINFO,
		REPORTS;
		
	}
	
}
