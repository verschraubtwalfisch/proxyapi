package net.nerux.proxapi.minecraft;

import java.util.List;
import java.util.UUID;

import io.netty.channel.Channel;
import net.nerux.proxapi.netty.NettyPacket;
import net.nerux.proxapi.netty.protocol.packets.StopServerPacket;
import net.nerux.proxapi.netty.protocol.packets.minecraft.ReloadServerPacket;
import net.nerux.proxapi.player.GlobalPlayer;

public abstract class MinecraftServer {

	public abstract String getServerName();
	
	public abstract String getMessageOfTheDay();
	
	public abstract String getMapName();

	public abstract String getServerStatus();
	
	public abstract Channel getChannel();
	
	public abstract int getPlayerCount();

	public abstract int getMaxPlayers();

	public abstract String getHost();
	
	public abstract int getPort();
	
	public abstract UUID getUUID();
	
	public abstract List<GlobalPlayer> getPlayers();

	public void stopServer(String reason){
		sendPacket(new StopServerPacket(reason));
	}
	
	public void reloadServer(String reason){
		sendPacket(new ReloadServerPacket(reason));
	}
	
	public void sendPacket(NettyPacket packet) {
		getChannel().writeAndFlush(packet);
	}

}
